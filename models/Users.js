import {Model, DataTypes} from "sequelize";
import sequelize from "../services/sequelize";

class Users extends Model {

}
Users.init({
    id:{
        type:DataTypes.BIGINT,
        autoIncrement:true,
        primaryKey:true,
    },
    name:{
        type:DataTypes.STRING,
        allowNull: false,
    },
    lName:{
        type:DataTypes.STRING,
        allowNull: false,
    },
    email:{
        type:DataTypes.STRING,
        allowNull: false,
        unique:true,
    },
    phone:{
        type: DataTypes.STRING(32),
        allowNull: false

    },
   profession: {
        type: DataTypes.STRING,
        allowNull: true,
    },
},
    {
        sequelize,
        modelName:'users',
        tableName: 'users'
    })

export  default Users
