import {Users} from "../models";
import {HttpError} from "http-errors";
import validate from "../services/validate";

class UsersController {
    static userRegister = async (req, res, next) => {
        try {
            const {
                name,
                lName,
                email,
                profession,
                phone
            } = req.body

            validate(req.body, {
                name: 'required|string|between:2,12',
                lName: 'required|string|between:2,12',
                email: 'required|email',
                phone: ['required', 'regex:/^\\(?[+]?([0-9]{3})\\)?[-]?([0-9]{2})?[-]?([0-9]{3})[-]?([0-9]{3})$/'],
                profession: 'required|string|between:2,10',
            }).throw();

            const existUser = await Users.findOne(
                {
                    where: {
                        $or: [
                            {email},
                            {phone},
                        ],
                    },
                },
            );

            if (existUser) {
                const errors = {}
                if (existUser.email === email) {
                    errors.email = ['Email must be unique'];
                }
                if (existUser.phone === phone) {
                    errors.phone = ['Phone must be unique'];
                }
                throw HttpError(422, {errors});
            }
            const user = await Users.create({
                name,
                lName,
                email,
                profession,
                phone
            })
            res.json({
                status: 'ok',
                user
            })

        } catch (e) {
            next(e)
        }

    }

    static getUsers = async (req, res, next) => {
        try {
            const users = await Users.findAll({});

            res.json({
                status: 'ok',
                users
            })
        } catch (e) {
            next(e)
        }
    }

    static updateUsers = async (req, res, next) => {

        try {
            const {id} = req.body;
            const {
                name,
                lName,
                email,
                phone,
                profession
            } = req.body;

            validate(req.body, {
                name: 'required|string|between:2,12',
                lName: 'required|string|between:2,12',
                email: 'required|email',
                phone: ['required', 'regex:/^\\(?[+]?([0-9]{3})\\)?[-]?([0-9]{2})?[-]?([0-9]{3})[-]?([0-9]{3})$/'],
                profession: 'required|string|between:2,10',
            }).throw();
            const users = await Users.findByPk(id);
            users.name = name;
            users.lName = lName;
            users.email = email;
            users.phone = phone;
            users.profession = profession;

            await users.save();

            res.json({
                status: 'ok',
                users,
            });
        } catch (e) {
            next(e);
        }
    };

    static deleteUser = async (req, res, next) => {
        try {
            const {uId} = req.params;
            const deleteUser = await Users.findOne({
                where: {id: uId},
            });
            await deleteUser.destroy();

            res.json({
                status: 'ok',
            });
        } catch (e) {
            next(e);
        }
    }
    static getSingleUser = async (req, res, next) => {
        try {
            const {id} = req.params;
            const singleUser = await Users.findOne({
                where: {id},
            });
            res.status(200).send({
                status: 'ok',
                singleUser,
            });
        } catch (e) {
            next(e);
        }
    };

}

export default UsersController
