import {Users} from '../models';

async function main() {
    try {
        await Users.sync({ alter: true });
        process.exit(0)
    } catch (e) {
        console.log(e)
    }
}

main();
