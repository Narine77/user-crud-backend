import express from "express";
import UsersController from "../controllers/UsersController";

const router = express.Router();

router.post('/add-user', UsersController.userRegister);
router.get('/users-list', UsersController.getUsers);
router.get('/single-user/:id', UsersController.getSingleUser);
router.put('/update-user', UsersController.updateUsers);
router.delete('/delete/:uId', UsersController.deleteUser);

export default router;
